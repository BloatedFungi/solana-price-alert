from pydub import AudioSegment
from pydub.playback import play


class PriceOutaRangeException(Exception):
    pass


def play_sound(file_path, volume_adjustment=0):
    sound = AudioSegment.from_file(file_path)
    sound = sound + volume_adjustment  # Adjust volume (negative values to decrease volume)
    play(sound)