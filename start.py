import importlib
import time
import argparse
import datetime
import traceback

import config
from utils import PriceOutaRangeException, play_sound


def main(symbol, min_price, max_price):

    for provider in config.provider_list:
        print(f'Trying to get price for {symbol} on {provider}')
        try:

            monitor(provider, symbol, min_price, max_price)
        except PriceOutaRangeException as e:
            print(e)
            exit()
        except Exception as e:
            print(f'Could not find it.  Next {e}')
            continue

    print(f'We could not find token ({symbol}) price on any of those {config.provider_list}')
    print(f'GLHF, WAGMI')


def monitor(provider, symbol, min_price, max_price):
    price = get_price(provider, symbol)
    if min_price <= price <= max_price:
        while True:
            try:
                price = get_price(provider, symbol)
                now = datetime.datetime.now()
                print(
                    f"{now} -- The current price of {symbol} is ${price}.  Monitoring for range {min_price} - {max_price}")

                if price <= min_price:
                    print(f"The price of {symbol} has dropped to ${price}! AAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHH!!!!")
                    play_sound('AAAAAAAAAAAHHH.wav')

                if price >= max_price:
                    print(f"The price of {symbol} has reached ${price}! HEY Song!")
                    play_sound('hey_song.mp3')

                time.sleep(40)
            except Exception as e:
                # uncomment for debug and stack trace
                #print(e)
                raise e
    else:
        raise PriceOutaRangeException("The price is already out of range.  Quitting.")


def get_price(provider, symbol):
    module_path = config.provider_list.get(provider)
    if not module_path:
        raise Exception(f"Unknown provider: {provider}")

    try:
        module = importlib.import_module(module_path)
        return module.get_token_price(symbol)
    except Exception as e:
        # uncomment for debug and stack trace
        #print(e)
        #traceback.print_exc()
        raise Exception(f"Failed to get price from {provider}: {e}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Monitor cryptocurrency prices.')
    parser.add_argument('symbol', type=str, help='The symbol of the cryptocurrency (e.g., SOL)')
    parser.add_argument('min_price', type=float, help='The minimum price threshold')
    parser.add_argument('max_price', type=float, help='The maximum price threshold')

    args = parser.parse_args()

    main(args.symbol, args.min_price, args.max_price)
