import requests

import config


def get_token_price(symbol, network='solana'):
    pair = None

    for token in config.token_list:
        if token['symbol'] == symbol:
            pair = token['pair']

    if pair is None:
        raise Exception("Could not find pair address in config.py.  Add the token and it's pair address as dexscreener's API needs it.")

    url = f'https://api.dexscreener.com/latest/dex/pairs/{network}/{pair}'
    response = requests.get(url)
    data = response.json()

    if 'priceUsd' in data['pairs'][0]:
        return float(data['pairs'][0]['priceUsd'])
    else:
        raise Exception("Price data not found for the given token address")


