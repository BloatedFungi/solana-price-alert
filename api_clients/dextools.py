import requests

import config


def get_token_price(symbol, network='solana'):
    token_address = None
    
    for token in config.token_list:
        if token['symbol'] == symbol:
            token_address = token['address']
            
    if token_address is None:
        raise Exception("Could not find token in config.  Place it there with it's address as Dextools API needs it.")
    
    url = f'https://api.dextools.io/v2/token/{network}/{token_address}'
    response = requests.get(url)
    data = response.json()

    if 'priceUsd' in data:
        return data['priceUsd']
    else:
        raise Exception("Price data not found for the given token address")


