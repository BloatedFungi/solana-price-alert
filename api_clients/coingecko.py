import requests


def get_token_price(symbol):
    token_id = get_coin_id(symbol)
    url = f'https://api.coingecko.com/api/v3/simple/price?ids={token_id}&vs_currencies=usd'
    response = requests.get(url)
    data = response.json()

    if token_id in data and 'usd' in data[token_id]:
        return float(data[token_id]['usd'])
    else:
        raise Exception("Price data not found for the given token ID")


def get_coin_id(symbol):
    url = 'https://api.coingecko.com/api/v3/coins/list'
    try:
        response = requests.get(url)
        coins = response.json()
    except Exception as e:
        print('Unable to list coin ids, try switching IP address. https://api.coingecko.com/api/v3/coins/list')
        raise e

    for coin in coins:
        if coin['symbol'].lower() == symbol.lower() and coin['id'].lower().startswith('solana'.lower()):
            return coin['id']
        elif coin['symbol'].lower() == symbol.lower() and coin['id'].lower().startswith('ethereum'.lower()):
            return coin['id']

    raise Exception(f"Coin ID not found {symbol}")
