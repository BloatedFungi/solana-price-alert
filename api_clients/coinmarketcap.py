# Function to read the API key from the file
import requests


def read_api_key(file_path):
    with open(file_path, 'r') as file:
        return file.read().strip()


API_KEY = read_api_key('api_clients/coinmarketcap-api-key.txt')


def get_token_price(symbol):
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
    headers = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': API_KEY,
    }
    parameters = {
        'symbol': symbol,
        'convert': 'USD'
    }
    response = requests.get(url, headers=headers, params=parameters)
    data = response.json()
    if data['data'][symbol]:
        return data['data'][symbol]['quote']['USD']['price']
    else:
        raise Exception('Coin not found')
