# Solana Price Alert

This project monitors the price of a specific token on the Solana blockchain and plays a sound when the price crosses specified thresholds.

## Requirements

- Python 3.x
- `requests` library
- `pydub` library
- `argparse` library
- `ffmpeg` installed on your system

## Setup

1. **Create a virtual environment**:

    ```sh
    python3 -m venv venv
    ```

2. **Activate the virtual environment**:

    - Linux:

        ```sh
        source venv/bin/activate
        ```

3. **Install the dependencies**:

    ```sh
    pip install -r requirements.txt
    ```

## Configuration

Coin market cap
Open an account and in setting, security, you can generate one for free.
Put it in api_clients/coinmarketcap-api-key.txt
Once this is done, if your coin is on CoinMarketCap, you have nothing else to configure

For shitcoins, look at some examples in config.py (RFK, GME)
Dexscreener needs the pair id (in config.py) and you can find it on the desired pair page.  
ie. RAY https://dexscreener.com/solana/avs9ta4nwdzfpje9ggvnjmvhcqy3v9pgazuz33bfg2ra

Dextools requires a subscription, could not test.  Most likely needs an api-key too.

## Running the Script

To run the script, use the following command:

```sh
python start.py SOL 165 176
                 1   2   3
1: Symbol
2: min price
3: max price
```

## Feature request and contributions are welcome

Like my work?  Send me some Sol  
7cXsWpgZoi5RBwqQXSSS9Dn33keQjmc7KD8H6SLEYsYg