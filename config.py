
# address required by dextools
# pair required by dexscreener
token_list = [
    dict(symbol='SOL', address='So11111111111111111111111111111111111111112', pair=''),
    dict(symbol='JFK', address='EzRasdye3wnQ5ZGzLFhvp2L1TT42k3Qa3jFsoUibWh39', pair='9xPjKDQBotxzRoK9iE1uw7ZckbZZtF8X1gEUGGJUmu2p'),
    dict(symbol='MANEKI', address='25hAyBQfoDhfWx9ay6rarbgvWGwDdNqcHsXS3jQ3mTDJ', pair='2aPsSVxFw6dGRqWWUKfwujN6WVoyxuhjJaPzYaJvGDDR'),
    dict(symbol='GME', address='', pair='9tz6vYKiBDLYx2RnGWC5tESu4pyVE4jD6Tm56352UGte'),
    dict(symbol='BODEN', address='', pair='6UYbX1x8YUcFj8YstPYiZByG7uQzAq2s46ZWphUMkjg5'),
    dict(symbol='RAY', address='', pair='avs9ta4nwdzfpje9ggvnjmvhcqy3v9pgazuz33bfg2ra')
]

# Name of API provider : folder+file name to import.
provider_list = {
    'CoinGecko': 'api_clients.coingecko',
    'CoinMarketCap': 'api_clients.coinmarketcap',
    'DEX Screener': 'api_clients.dexscreener',
    'Dextools': 'api_clients.dextools'
}
